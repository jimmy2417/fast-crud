# crudOptions.settings
一些fs的设置

## viewFormUseCellComponent
* 说明：查看表单字段组件是否使用行展示组件来展示
* 类型：Boolean
* 默认：`false`


## searchCopyFormProps
* 说明：search字段从form配置中继承哪些属性
* 类型：string[]
* 默认：`["component", "valueChange","title", "key","label", "render"]`
